# Initgetdp

Directions to use the GetDP instance customized to CERN's needs on the STEAM VMs. 

1. Open a terminal
2. Clone this repository `git clone https://gitlab.cern.ch/eschnaub/initgetdp.git`
3. Source the initializing file `source initgetdp/initgetdp.sh`
4. Check if properly installed by running `which gmsh` and `which getdp`. Both should point to binaries on `eos/project/s/steam/sw`. 
5. Now you can run models in the gmsh GUI by running `gmsh modelname.pro` or the python files by running `python3 pythonfile.py`.  
