#!/bin/env bash
# get latest compiler
source /cvmfs/sft.cern.ch/lcg/releases/LCG_100/gcc/10.3.0.fp/x86_64-centos7/setup.sh

# switch to virtual python environment 
source /eos/project/s/steam/sw/gmshEnv/bin/activate

# create local binary if it doesn't exist
mkdir -p ~/bin 

# copy getdp binary to personal home due to socket listening issues on eos 
cp /eos/project/s/steam/sw/bin/getdp ~/bin 

# add binary to path if it is not there yet

pathadd() {
    if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
        PATH="${PATH:+"$PATH:"}$1"
    fi
}

pathadd "$HOME/bin"
